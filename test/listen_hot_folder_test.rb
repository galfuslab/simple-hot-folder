require 'test_helper'

module SimpleHotFolder
  class TestListenHotFolder < Minitest::Test

    def setup
      Test::initialize_test_folders!
      @hf = HotFolder.new(
        Test::FOLDERS[:input],
        Test::FOLDERS[:error]
      )
    end

    def test_listen_and_stop_after_the_first_item_is_processed
      i = 0
      filename = ''
      @hf.listen_input! do |item|
        i += 1
        filename = item.name
        @hf.stop_listening_after_this_item 
      end
      content = Test.content_of_folders
      assert_equal 1, i
      assert !content[:input].include?(filename)
      assert !content[:error].include?(filename)
    end

  end
end
